<?php

class m131110_083727_email extends CDbMigration
{
	public function up()
	{
        $this->addColumn('user', 'email', 'VARCHAR(60)');
	}

	public function down()
	{
		echo "m131110_083727_email does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}