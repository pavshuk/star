<?php

class m131110_073827_init extends CDbMigration
{
	public function up()
	{
        $this->createTable("user",
            array("id"=> "pk",
                "name"=>"VARCHAR(30)",
                "comment"=>"VARCHAR(50)"
            ));
	}

	public function down()
	{
		echo "m131110_073827_init does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}