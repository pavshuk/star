<?php
/* @var $this UserController */
/* @var $model User */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'user-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

    <?php
    $clip = array();

    $clip[]= $this->beginWidget('system.web.widgets.CClipWidget',array('id'=>'first')); ?>
	<div class="row">
		<?php echo $form->labelEx($model,'name'); ?>
		<?php echo $form->textField($model,'name',array('size'=>30,'maxlength'=>30)); ?>
		<?php echo $form->error($model,'name'); ?>
	</div>
    <div class="row">
        <?php echo $form->labelEx($model,'email'); ?>
        <?php echo $form->textField($model,'email',array('size'=>30,'maxlength'=>30)); ?>
        <?php echo $form->error($model,'email'); ?>
    </div>
    <?php $this->endWidget();?>

    <?php
    $clip[]= $this->beginWidget('system.web.widgets.CClipWidget',array('id'=>'second')); ?>


	<div class="row">
		<?php echo $form->labelEx($model,'comment'); ?>
		<?php echo $form->textField($model,'comment',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'comment'); ?>
	</div>
    <?php $this->endWidget();?>


 <?php
 $this->widget('zii.widgets.jui.CJuiTabs',array(
     'tabs'=>array(
         'first'=>$this->clips[$clip[0]->getId()],
         'second'=>$this->clips[$clip[1]->getId()]
     ),
     // additional javascript options for the tabs plugin
     'options'=>array(
         'collapsible'=>true,
     ),
 ));?>
    <div class="row buttons">
        <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
    </div>
<?php $this->endWidget(); ?>

</div><!-- form -->